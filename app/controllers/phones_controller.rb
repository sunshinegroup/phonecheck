class PhonesController < ApplicationController
    def index
        return "asdasdas"
    end

    def query
        if request.post?
            @phone = params['phone']

            if @phone.blank?
                flash[:alert] = "电话不能为空"
                render :index
                return
            end

            @found = Phone.where("phone like ?", "%#{@phone}%").first

            if @found.nil?
                flash[:alert] = "查不到"
                render :index
                return
            end

            flash.clear
            render :index
        else
            flash.clear
            redirect_to :action => :index
        end
    end
end
